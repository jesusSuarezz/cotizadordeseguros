import React, { useState } from 'react';

// Librerias de terceros
import styled from '@emotion/styled';

import Header from './components/Header';
import Formulario from './components/Formulario';
import Resumen from './components/Resumen';
import Resultado from './components/Resultado';
import Spinner from './components/Spinner';

const Contenedor = styled.div`
	max-width: 600px;
	margin: 0 auto;
`;

const ContenedorForm = styled.div`
	background-color: #fff;
	padding: 3rem;
`;
function App() {
	const [resumen, guardarResumen] = useState({
		cotizacion: 0,
		datos: {
			marca: '',
			year: '',
			plan: '',
		},
	});

	const [cargando, saveCargando] = useState(false);

	const { cotizacion, datos } = resumen;

	return (
		<Contenedor>
			<Header titulo="Cotizador de seguros (Jesús Suárez)" />

			<ContenedorForm>
				<Formulario
					guardarResumen={guardarResumen}
					saveCargando={saveCargando}
				/>

				{cargando ? <Spinner /> : null}

				<Resumen datos={datos} />

				{!cargando ? <Resultado cotizacion={cotizacion} /> : null}
			</ContenedorForm>
		</Contenedor>
	);
}

export default App;
